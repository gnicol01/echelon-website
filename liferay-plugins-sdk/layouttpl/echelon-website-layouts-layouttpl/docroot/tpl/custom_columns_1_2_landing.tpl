<div class="custom_columns_1_2_landing" id="main-content" role="main">


	<div class="portlet-layout">
		<div class="portlet-column portlet-column-only" id="column-1">
			$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
		</div>
	</div>
	<div id="templateContainer">
		<div id="template" class="hasLeftCol container-fluid">
			<div class="portlet-layout row-fluid">
				<div id="left" class="">
					<div class="portlet-column portlet-column-first" id="column-2">
						$processor.processColumn("column-2", "portlet-column-content portlet-column-content-first")
					</div>
				</div>
				<div class="portlet-column portlet-column-last" id="column-3">
					$processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")
				</div>
				<div class="clearer"></div>
			</div>
	
		</div>
	</div>
	
</div>
