<div class="custom_columns_1_2_home" id="main-content" role="main">
	<div id="homeContainer">
		<div class="portlet-layout">
			<div class="portlet-column portlet-column-only" id="column-1">
				$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
			</div>
		</div>
		<div id = "home">
			<div class="portlet-layout row-fluid">
				<div class="aui-w50 portlet-column portlet-column-first span8" id="column-2">
					$processor.processColumn("column-2", "portlet-column-content portlet-column-content-first")
				</div>
				<div class="aui-w50 portlet-column portlet-column-last span4" id="column-3">
					$processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")
				</div>
			</div>
		</div>
	</div>
</div>
