<div class="custom_columns_1_1" id="main-content" role="main">
	<div class="portlet-layout">
		<div class="portlet-column portlet-column-only" id="column-1">
			$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
		</div>
	</div>
	<div id="templateContainer">
		<div id="template" class="hasOneCol container-fluid">
			<div class="portlet-layout row-fluid">
				<div class="portlet-column portlet-column-last" id="column-2">
					$processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
				</div>
				<div class="clearer"></div>
			</div>
	
		</div>
	</div>
	
</div>
