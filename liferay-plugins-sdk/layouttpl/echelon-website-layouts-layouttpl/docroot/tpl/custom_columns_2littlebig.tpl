<div class="custom_columns_2littlebig" id="main-content" role="main">


	<div id="templateContainer">
		<div id="template" class="hasLeftAndRightCols">

			<div class="portlet-layout">
				<div class="aui-w70 portlet-column portlet-column-first" id="column-1">
					$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
				</div>
				<div class="aui-w30 portlet-column portlet-column-last" id="column-2">
					$processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
				</div>
			</div>
		</div>
	</div>
</div>
