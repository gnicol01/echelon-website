<div id="contentContainer">
	<div class="custom_1_2_columns_blog" id="main-content" role="main">
	
		<div class="portlet-layout">
			<div class="portlet-column portlet-column-only" id="column-1">
				$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
			</div>
		</div>
		<div id="templateContainer">
			<div id="template" class="hasRightCol">
				<div class="portlet-layout">
					<div id="right">
						<div class="aui-w75 portlet-column-first" id="column-2">
							$processor.processColumn("column-2", "portlet-column-content portlet-column-content-first")
						</div>
					</div>
					<div id="sidebar">
						<div class="aui-w25 portlet-column-last" id="column-3">
							$processor.processColumn("column-3", "portlet-column-content portlet-column-content-last")
						</div>
					</div>
				<div class="clearer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
