// Stop jQuery mobile from appending a loading div to the bottom of our html body
$.mobile.autoInitializePage = false;

$(document).ready(function(){
	var isHamburgerMenuOpen = function() {
		return ($('.mobile-overlay').css("display") == 'block');
	}
	var closeHamburgerMenu = function() {
		if (isHamburgerMenuOpen()) {
			$('#hamburgerMenu-inner').animate({
				right: "-500px"
			}, 500);
			$('.mobile-overlay').css("display", "none");
		}
	}
	
	var openHamburgerMenu = function () {
		if (!isHamburgerMenuOpen()) {
			$('#hamburgerMenu-inner').css("right", "-500px");
			$('#hamburgerMenu-inner').animate({
				right: "0px"
			}, 500);
			$('.mobile-overlay').css("display", "block");
		}
	}

	$('#MightyMenu > ul > li > a').click(function(){
		$('#MightyMenu > ul > li > a').removeClass('activeTrail');
		$(this).addClass('activeTrail');
	});
	
	$('#hamburgerMenu-open').on("click", openHamburgerMenu);
	
	$('#hamburgerMenu-close').on("click", closeHamburgerMenu);
	
	$('#hamburgerMenu-inner').on("swiperight", function() {
		closeHamburgerMenu();
	});
	
	$('.swipeOpenMenuDiv').on("swipeleft", function() {
		openHamburgerMenu();
	});
	
	$('.mobile-overlay').on("click swiperight", function () {
		closeHamburgerMenu();
	});
	
	$('div.caretHolder').on("click touchstart", function() {
		var caret = $(this).find('#caret');
		var nextLevelNav = $(this).siblings('#nextNav');
		if (caret.hasClass('caret')) {
			caret.removeClass('caret');
			caret.removeClass('caret-zoomed');
			caret.addClass('caret-right');
			nextLevelNav.removeClass('expanded');
			nextLevelNav.addClass("collapsed");
		} else {
			caret.removeClass('caret-right');
			caret.removeClass('caret-right-zoomed');
			caret.addClass('caret');
			nextLevelNav.removeClass('collapsed');
			nextLevelNav.addClass("expanded");
		}
		return false;
	});
	
	$("#showingCategoryDelete").on("click", function() {
		var oldUrl = $(location).attr("href");
		var newUrl = updateQueryStringParameter(oldUrl, "categoryId", 0);
		window.location.href = window.location.pathname;
		return false;
	});
	$("#showingTagDelete").on("click", function() {
		var oldUrl = $(location).attr("href");
		var newUrl = updateQueryStringParameter(oldUrl, "tag", '');
		window.location.href = window.location.pathname;
		return false;
	});
	
	function updateQueryStringParameter(uri, key, value) {
		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			return uri.replace(re, '$1' + key + "=" + value + '$2');
		}
		else {
			return uri + separator + key + "=" + value;
		}
	}
	
	var iPhoneSideWaysImage = function() {
		if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
			$("img").each(function( index ) {
				if ($(this).prop("src").indexOf("JeffStanislaw") >= 0) {
					rotate( $(this), 270 );
				}
			});
		}
	}
	
	function rotate( img, degree ) {
		$(img).css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
	      // For Mozilla browser: e.g. Firefox
	    $(img).css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
	    $(img).css({ 'transform': 'rotate(' + degree + 'deg)'});
	}
	iPhoneSideWaysImage();
	
	if ($("#left").length) {
		$("#left").stick_in_parent();
	}
});