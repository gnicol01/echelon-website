$(document).ready(function(){
	
	var first = $('#slideshow .slide:first');
	var sliding = false;
	
	first.addClass('activeSlide');
	first.css("display", "inline");
	
	$('#slideshow_actions_btns #navButtons .ro:first a').addClass('activeSlide');
	
	$('#slideshow_actions_btns #navButtons .ro a img').click(function(){
	
		var currentThumb = $('#slideshow_actions_btns #navButtons .ro .activeSlide');
		var slideId = $(this).attr('alt');
		var nextThumb = $('#navButtons').find('.' + slideId).find('a');
		
		if(currentThumb[0] != nextThumb[0] && !sliding)
			{
				clearInterval(slideshowFeature);
			
				activeSlide = $('#slideshow .activeSlide');
				sliding = true;
				activeId = activeSlide.attr('id');
	
				var nextSlide = $('#slideshow').find('#' + slideId);
				
				event.preventDefault ? event.preventDefault() : event.returnValue = false;;
				changeSlideAction(activeSlide, nextSlide, currentThumb, nextThumb);
				
				
				slideshowFeature = setInterval(function(){changeSlide();}, 4000);
			}
	});

	var slideshowFeature = setInterval(function(){changeSlide();}, 5000);
	
	
	var changeSlide = function () {
		activeSlide = $('#slideshow .activeSlide');
		currentThumb = $('#slideshow_actions_btns #navButtons .ro .activeSlide').parent();
		
		if(activeSlide.next('.slide').length > 0){
			nextSlide = activeSlide.next('.slide');
			
		}
		else{
			nextSlide = $('#slideshow .slide:first');
		}
		if(currentThumb.next('.ro').length > 0){
			nextThumb = currentThumb.next('.ro');
		}
		else{
			nextThumb = $('#slideshow_actions_btns #navButtons .ro:first');
		}

		changeSlideAction(activeSlide, nextSlide, currentThumb.find('a'), nextThumb.find('a'));
			
	};
		
	var changeSlideAction = function(activeSlide, nextSlide, currentThumb, nextThumb){
		//clearInterval(slideshowFeature);
		if ($(".mobile-overlay").css('display') == 'none') {
			activeSlide.removeClass('activeSlide');
			nextSlide.css("left", "1000px");
			nextSlide.css("display", "inline");
			nextSlide.addClass('activeSlide');
		
			nextSlide.animate({left: 0}, 1000, function() {
				activeSlide.css("display", "none");
				currentThumb.removeClass('activeSlide');
				nextThumb.addClass('activeSlide');
				sliding = false;
			});
		}

//		slideshowFeature = setInterval(function(){changeSlide();}, 4000);
		
	};
	
	/*$('#slideshow').cycle({
		fx: 'scrollLeft',
		sync: true,
		skipInitializationCallbacks: true
	});*/
});




