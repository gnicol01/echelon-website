

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %><%@
taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="com.liferay.portal.model.Group" %>
<%@ page import="com.liferay.portal.kernel.util.UnicodeProperties" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%

Group scopeGroup = themeDisplay.getScopeGroup();
String pubKey = scopeGroup.getExpandoBridge().getAttribute("Disqus Public API Key").toString();
UnicodeProperties typeSettingsProperties = scopeGroup.getTypeSettingsProperties();
String disqusShortName = typeSettingsProperties.getProperty("social-comments-disqus-short-name");


%>

<div class="widget recentComments">
	<div class="title">Recent Comments</div>
	<ul id="recentCommentsContainer">
	</ul>
	<input type="hidden" name="disqus_key" value="<%=pubKey %>" />
	<input type="hidden" name="disqus_shortname" value="<%=disqusShortName %>" />
</div>
