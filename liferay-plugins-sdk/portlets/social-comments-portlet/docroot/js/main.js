$(document).ready(function(){
	MAX_WORDS = 10;
	MAX_COMMENTS = 4;
	PUB_API_KEY = $("input[name='disqus_key']").val();
	FORUM = $("input[name='disqus_shortname']").val();
	RELATED = "thread";
	DISQ_URL = "https://disqus.com/api/3.0/forums/listPosts.json?" + 
				"forum=" +
				FORUM + 
				"&related=" +
				RELATED +
				"&api_key=" +
				PUB_API_KEY;
	
	var entityMap = {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;",
			'"': '&quot;',
			"'": '&#39;',
			"/": '&#x2F;'
	};

	function escapeHtml(string) {
		return String(string).replace(/[&<>"'\/]/g, function (s) {
			return entityMap[s];
		});
  	}
	
	var createRecentCommentBlock = function(author, articleTitle, body, date, articleURL, commentURL, profileURL) {
		var $commentBlock = $("<li>" +
				"<a class='commentAuthor' href=" + escapeHtml(profileURL) + ">" +
				author +
				"</a>" +
				" on " +
				"<a href=" + escapeHtml(articleURL) + ">" +
				articleTitle +
				"</a>" +
				' said:' +
				"</span>" +
				"<div class='commentBody'>" +
				"<a href=" + escapeHtml(commentURL) + ">" +
				'"' +
				body +
				'"' +
				"</a>" +
				"</div>" +
				"<div class='commentDate'>" +
				date +
				"</div>" +
				"</li>");
		
		$("#recentCommentsContainer").append($commentBlock);
	};
	
	var formatBody = function (body) {
		var array = body.split(" ");
		var wordCount = array.length;
	    var string = array.splice(0, MAX_WORDS).join(" ");
	    
	    if(wordCount > MAX_WORDS) {
	        string += "...";
	    }

	    return string ;
	}
	
	var getMostRecentComments = function() {
		$.get(DISQ_URL, function(data) {
			var response = data['response'];
			var haveComments = false;
			var counter = 0;
			$.each(response, function(index, value) {
				counter += 1;
				if (counter > MAX_COMMENTS) {
					return false;
				}
				haveComments = true;
				var commentURL = response[index]['url'];
				var articleURL = response[index]['thread']['link'];
				var articleTitle = response[index]['thread']['clean_title'];
				var commentAuthor = response[index]['author']['name'];
				var commentProfileUrl = response[index]['author']['profileUrl'];
				var avatarLink = response[index]['author']['avatar']['small']['permalink'];
				var comment = formatBody(response[index]['raw_message']);
				createdAt = moment.utc(response[index]['createdAt']).fromNow();

				createRecentCommentBlock(commentAuthor, articleTitle, comment, createdAt, articleURL, commentURL, commentProfileUrl);
			});
			
			if (!haveComments) {
				$("#recentCommentsContainer").append("<li>No comments to show</li>");
			}
		});
	};
	
	if ($(".recentComments").length) {
		getMostRecentComments();
		if ($("#sidebar").length) {
			$("#sidebar").stick_in_parent();
		}
	}
});