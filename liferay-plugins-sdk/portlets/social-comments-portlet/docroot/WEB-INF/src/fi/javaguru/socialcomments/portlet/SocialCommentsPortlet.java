package fi.javaguru.socialcomments.portlet;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.model.Group;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.GroupServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.util.bridges.mvc.MVCPortlet;
import java.io.IOException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

public class SocialCommentsPortlet
  extends MVCPortlet
{
  public void updateConfiguration(ActionRequest actionRequest, ActionResponse actionResponse)
    throws IOException, PortletException
  {
    try
    {
      ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
      
      long scopeGroupId = serviceContext.getScopeGroupId();
      
      Group scopeGroup = GroupLocalServiceUtil.getGroup(scopeGroupId);
      if (scopeGroup.isStagingGroup()) {
        scopeGroup = scopeGroup.getLiveGroup();
      }
      UnicodeProperties typeSettingsProperties = scopeGroup.getTypeSettingsProperties();
      
      UnicodeProperties properties = PropertiesParamUtil.getProperties(actionRequest, "settings--");
      
      typeSettingsProperties.putAll(properties);
      
      GroupServiceUtil.updateGroup(scopeGroup.getGroupId(), scopeGroup.getTypeSettings());
    }
    catch (Exception e)
    {
      SessionErrors.add(actionRequest, e.getClass().getName());
    }
  }
}
