package fi.javaguru.socialcomments.portlet;

import com.liferay.portal.model.Portlet;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portlet.BaseControlPanelEntry;

public class SocialCommentsControlPanelEntry
  extends BaseControlPanelEntry
{
  public boolean isVisible(PermissionChecker permissionChecker, Portlet portlet)
    throws Exception
  {
    return false;
  }
}
