 var validate = function (item)
    {
        if(item.trim())
        {
            return true;
        }
        else 
        {
            return false;
        }
        
    };
    var validateContactForm = function(form)
    {
    	
        var valid = true;
        var invalid = [];
        
        //Check required fields
        if(!validate(form.first_name.value))
        {
            valid = false;
            invalid.push('First Name');
        }
        if(!validate(form.last_name.value))
        {
            valid = false;
            invalid.push('Last Name');
        }
        if(!validate(form.email.value))
        {
            valid = false;
            invalid.push('Email');
        }
        if(!validate(form.comment.value))
        {
            valid = false;
            invalid.push('Comment');
        }
        
        if(!valid)
        {
        	var x;
        	var invalidString = '';
        	for(x=0; x< invalid.length; x++){
        		if(x==0){
        			invalidString = invalid[x];
        		}
        		else {
        			invalidString = invalidString + ', ' + invalid[x];
        		}
        			
        		
        	}
        		
        	alert("Please fill out the following fields: " + invalidString);
        }
        
        
        return valid;
    };