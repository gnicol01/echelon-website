<%
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@page import= "com.echelon.portlet.SendEmail" %>


<portlet:renderURL var="submitURL">
 <portlet:param name="jspPage" value="/contactUsReceiver.jsp" />
</portlet:renderURL>

<portlet:defineObjects />

            	<h1>Contact</h1>
              <p>If you have an inquiry or would like us to get in touch with you, kindly fill in your information below and a team member will be in touch.</p>
              <div class="sendFormContainer">
                <form class="sendForm contactForm" name="contact" method="POST" action="<%= submitURL %>" onsubmit="return validateContactForm(this)">
                  <input type="hidden" name="<portlet:namespace />form_id" value="1">
                  <div class="sendFormHead">
                    <div class="sendformHead_content">
                      <h2>Send Us a Message</h2>
                      <span class="required">All fields required unless otherwise noted.</span>
                    </div>
                  </div>
                  <div class="sendFormRow">		
                    <label for="first_name">First Name</label>
                    <input name="<portlet:namespace />first_name" id="first_name" value="" class="input" type="text" tabindex="1">
                  </div>
                  <div class="sendFormRow">
                    <label for="last_name">Last Name</label>
                    <input name="<portlet:namespace />last_name" id="last_name" value="" class="input" type="text" tabindex="2">
                  </div>
                  <div class="sendFormRow">
                    <label for="email">Email</label>
                    <input name="<portlet:namespace />email" value="" id="email" class="input" type="text" tabindex="3">
                  </div>
                  <div class="sendFormRow">
                    <label for="phone">Phone <span class="optional">(optional)</span></label>
                    <input name="<portlet:namespace />phone" value="" id="phone" class="input" type="text" tabindex="4">
                  </div>
                  <div class="sendFormRow">		
                    <label for="comment">Comment</label>
                    <textarea name="<portlet:namespace />comment" id="comment" tabindex="5"></textarea>
                  </div>
                  <div class="sendFormRow submitBtnRow">
                    <input type="submit" name="submit" value="Submit Comment" class="button submit" tabindex="6">
                  </div>
                </form>	
                </div>
